# Clean up FODT text
# Remove xml:id="list12345" and lossy PNG images
sed -e 's/ xml:id="list[0-9]*"//' $1 | awk '/<draw:image loext:mime-type="image\/png">/{p=1}/<\/draw:frame>/{p=0}!p'