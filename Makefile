DIRS = public/en public/ru

FODT_INPUT = $(foreach DIR, $(DIRS), $(wildcard $(DIR)/*.fodt))

FORMATS := pdf html doc docx

all: $(FORMATS)

.SECONDEXPANSION:
$(FORMATS): $(patsubst %.fodt, %.$$@, $(FODT_INPUT))

%.pdf: %.fodt
	unoconv -f pdf "$<"

%.doc: %.fodt
	unoconv -f doc "$<"

%.docx: %.fodt
	unoconv -f docx "$<"

# XHTML output looks better than HTML. XHTML files have .html extension
%.html: %.fodt
	unoconv -f xhtml "$<"
	sed -i  "s/ left\:3\./ left:3,/" "$@"

# Normalize to make idempotent before commit
norm: $(FODT_INPUT)
	@for f in $(FODT_INPUT); do \
		xsltproc -o "$$f".norm norm_fodt_level_1.xsl "$$f" ;\
		mv "$$f" "$$f".orig ;\
		sh norm_fodt_level_2.cmd "$$f".norm > "$$f" ;\
	done

clean:
	rm -f $(foreach FORMAT, $(FORMATS), $(foreach INPUT, $(FODT_INPUT), $(patsubst %.fodt, %.$(FORMAT), $(INPUT)) $(INPUT).orig $(INPUT).norm))
